package soap

import (
	"gitlab.fideliscare.org/goservices/utility/shared/scope"
	"gitlab.fideliscare.org/goservices/utility/soap"
)

//CallOne ...
func CallOne(paramOne string, requestScopeData scope.ContextData) ([]byte, error) {
	soapReqTemplate := ``
	//Alter the struct according to soap template
	reqData := struct {
		one      string
		region   string
		identity string
	}{
		one:      paramOne,
		region:   requestScopeData["inboundHostToHTTPConfig"]["Region"],
		identity: requestScopeData["inboundHostToHTTPConfig"]["Identity"],
	}
	//If host need to be added based on config
	host := requestScopeData["inboundHostToHTTPConfig"]["Protocol"] + "://" + requestScopeData["inboundHostToHTTPConfig"]["Host"]
	res, err := soap.Do(soapReqTemplate, reqData, "soapMethod", host)
	// If no need of host to be added based on config
	// res, err := soap.Do(soapReqTemplate, reqData, "soapMethod", nil)

	return res, err
}
