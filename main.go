package main

import (
	"gitlab.fideliscare.org/goservices/service-name/handler"
	"gitlab.fideliscare.org/goservices/utility/http/server"
	"gitlab.fideliscare.org/goservices/utility/logger"
)

var serverLog = logger.GetLogger()

func main() {
	router, httpServer := server.GetRouterAndServer()
	router.HandleFunc("/endpoint-path", handler.Endpoint)
	if err := httpServer.ListenAndServe(); err != nil {
		serverLog.Fatal(err)
	}
}
