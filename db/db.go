package db

import (
	"fmt"

	"gitlab.fideliscare.org/goservices/utility/database/oracle"
)

//SprocOne ...
func SprocOne(paramOne int, paramTwo string) ([]byte, error) {
	query := "Databaase.SprocName(:%d, :%s)"
	execQuery := fmt.Sprintf(query, paramOne, paramTwo)
	res, err := oracle.ExecQueryByCommand(execQuery, "dbConnectionName")
	return res, err
}
