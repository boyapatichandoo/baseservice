package handler

import (
	"net/http"

	"gitlab.fideliscare.org/goservices/utility/logger"
	"gitlab.fideliscare.org/goservices/utility/shared/resources"
)

var handlerLog = logger.GetLogger()

//EndpointOne handles the "/" endpoint
func EndpointOne(w http.ResponseWriter, r *http.Request) {
	requestScopeData, err := resources.InitService()
	if err != nil {
		handlerLog.Error(err)
		return
	}
	//handler logic
	resources.EndServiceLog("http status", "error if any", requestScopeData)
}
